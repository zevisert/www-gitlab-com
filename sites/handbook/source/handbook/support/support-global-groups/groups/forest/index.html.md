---
layout: handbook-page-toc
title: Home Page for SGG Forest - for all Support Managers and SGG Team Members
description: Home Page for SGG Forest - for all Support Managers and SGG Team Members
---

<!-- Search for all occurrences of NAME and replace them with the group's name.
     Search for all occurrences of URL HERE and replace them with the appropriate url -->

# Welcome to the home page of Forest 

While Forest isn't an actual Support Global Group, the Forest Slack channel gives managers an opportunity to work more closely together as a "global group" in support of SGG. Through the channel managers have increased visibility on things like SGG and manager capacity, and can strategize on global and regional SGG initiatives.

Additionally, all SGG team members are encouraged to collaborate with managers in Forest to discuss ideas, challenges and successes, and to seek help when needed.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Forest resources

- Main Slack Channel: [spt_gg_forest](https://gitlab.slack.com/archives/C03LL7Z2291)

## Forest workflows and processes

### Daily Manager Standup (Optional)
Each day, much like for each Support Global Group, a bot notifies the channel of regional manager availability and of SGG and on-call details (including US Fed for the AMER region). This starts the daily thread where managers can highlight:
- individual project and task focuses for the day
- announcements
- requests for help from other managers
- SGG capacity concerns and "things to be aware of"
- escalation or emergency handovers
- any other requests or items that managers wish to discuss related to that day

Additionally, managers are encouraged to use the information from the bot to make decisions when finding assignees for escalated tickets, managing simultaneous emergencies, or strategizing on capacity-related concerns for a given SGG and the impact that may have on FRT/NRT focuses.

### Collaboration

Future ideas and iterations to increase both manager and SGG team member collaboration in this group are welcome.

### Escalations

On-call managers can summarize any escalations that require next region attention. This is a good area where we can iterate/evolve further.
