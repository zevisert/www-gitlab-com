---
layout: handbook-page-toc
title: "Hiring Product Designers"
description: "Product Designers, Product Design Managers, the Director of Product Design, and Product Managers participate in our hiring process by interviewing Product Designer candidates. We have created guidelines to help support a consistent end-to-end hiring process."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
{::options parse_block_html="true" /}

- TOC
{:toc .hidden-md .hidden-lg}


## Hiring process
Product Designers follow the [Engineering hiring process](https://about.gitlab.com/handbook/engineering/#hiring-practices).

General resources:
- [Interviewing at GitLab](https://about.gitlab.com/handbook/hiring/interviewing/#conducting-a-gitlab-interview)
- [Product Designer job family](https://about.gitlab.com/job-families/engineering/product-designer/)

## Interviews for Product Designers
Each Product Designer candidate is interviewed by a panel of interviewers that include:
- Product Designer
- Product Design Manager
- Director of Product Design
- Product Manager

A [detailed description of each stage of our interview process](https://about.gitlab.com/job-families/engineering/product-designer/#hiring-process) is available along with the job description.

## Interview training
Any Product Designer can participate in the hiring process after completing the [company interview training](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/interview_training.md) and a Product Design-specific interview training. These trainings will teach you how to perform successful interviews at GitLab.

In the last steps of the training, you will:
- Shadow an experienced Product Designer interviewer in two separate interviews, so that you can see how a successful interview is conducted. 
- Be shadowed by your coach in two separate interviews. They will act as your coach and provide feedback on your interviewing skills. 

You may request additional shadow opportunities, if you'd like additional practice and support.

### Role as an Interview trainee
_Details to be added_

### Role as an Expert interviewer
_Details to be added_
