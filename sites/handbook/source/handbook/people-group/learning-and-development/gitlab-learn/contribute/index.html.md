---
layout: handbook-page-toc
title: Contribute to GitLab Learn
description: "Learn how team members and our wider community can contribute learning content to the Learning Experience Platform (LXP)."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Handbook First Learning Content in GitLab Learn

Content in the LXP is created using a handbook first approach to learning. This means that all learning content that users can access in the LXP can be found in the handbook. A [handbook first approach to learning](/handbook/people-group/learning-and-development/interactive-learning) in the LXP ensures:

- all voices and contributions are heard
- barriers to contributions are removed or reduced
- the organization maintains a single source of truth

## Contribute to the LXP

The contribution process is currently under review. If you would like to create learning material, please contact the Learning and Development team in the #learninganddevelopment Slack channel, or email us at learning@gitlab.com. 

## Learning content in the LXP

It's important for all content contributors to understand how learning material is created and organized in GitLab Learn. Review the following notes and videos to get a sense of how the platform is built and how content is shared with and discovered by users.

EdCast organizes content in the LXP in three major types of content buckets. When contributing to the LXP, it's important to consider which of these three types might be best to organize content and guide learners to their learning objectives.

<iframe width="560" height="315" src="https://www.youtube.com/embed/dx_nev99a9o" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### SmartCards

SmartCards are the basic unit of information in the LXP. They are used to direct learners to explore specific pieces of content in the handbook, watch recorded videos, or complete quick knowledge checks and assessments. Content in the LXP is built and arranged with SmartCards.

#### Examples of SmartCard content

- Links to GitLab handbook pages, documentation pages, and YouTube videos
- Links to external articles and resources that can supplement handbook resources
- Advertising/sharing a live Learner Speaker Series being hosted for GitLab team members, or a public AMA with our Product team. These can also be used for members to sign up to attend live learning sessions
- Polling a group of users to get their feedback on an interactive course on Emotional Intelligence
- Assessing learnings with a short quiz/knowledge check after reading a set of handbook pages
- Space for learners to upload and share submissions to projects, like a slide deck or animated video they've created

![smart-card-example](/handbook/people-group/learning-and-development/gitlab-learn/contribute/smartcard.png)

### Pathway

A Pathway is a collection of SmartCards organized as a learning path.

#### Examples of Pathway content

- A set of 6 handbook pages (SmartCards) readers should review in sequence to gain a stronger understanding of one topic

![pathway-example](/handbook/people-group/learning-and-development/gitlab-learn/contribute/pathway.png)


### Journey

A Journey is a structured sequence of SmartCards and Pathways.

#### Examples of Journey content

- A set of multiple pathways a user must work through in order to earn a certification, for example GitLab 101 or GitLab 201


## Organizing content in the LXP

Content in the LXP is organized using the following structures. Learners use these structures to discover new content.

### Carousels

Carousels are horizontal containers of related channels or content assets.

### Examples of Carousel organization

- A carousel could be curated to display 3 different journey's related to Emotional Intelligence

![carousel-example](/handbook/people-group/learning-and-development/gitlab-learn/contribute/carousel.png)


### Channels

Channels are the principal way in which content (SmartCards, Pathways, and Journeys) are broadcast to learners throughout the LXP. Channels can be customizable based on the vision of the learners using the LXP. 

#### Examples of Channel organization

- A series of SmartCards, Pathways, and Journeys on Agile Project Management

![channel-example](/handbook/people-group/learning-and-development/gitlab-learn/contribute/channel.png)


### Group

Groups are a collection of people with similar characteristics or learning interests.

#### Examples of Group organization

- The Marketing team organizes a group of program managers to participate in a specific journey

![group-example](/handbook/people-group/learning-and-development/gitlab-learn/contribute/group.png)

